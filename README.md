## mysqldumper

### Backup

- Clone this repository to your server
- Copy `mysqldumper.default.conf` to `mysqldumper.conf`
- Fill MySQL server & login information which need to run mysqldump command
- Execute `mysqldumper.sh`

### Restore

- Copy `mysqldumper.default.conf` to `mysqlimport.conf`
- Execute `mysqlimport.sh BASE_BACKPUP_DIR` 

### Use mysql_config_editor

Instead of filling mysql credentials, we can use mysql_config_editor to setup passwordless mysql user.

```
mysql_config_editor set -G backup --host=127.0.0.1 --user=backup --password
```

Then setting up login_path key to above value (backup).
