#!/bin/bash 

SCRIPT_FULLPATH=$(readlink -f "$0")
SCRIPT_DIR=$(dirname "$SCRIPT_FULLPATH")
CONFIGFILE_NAME="${SCRIPT_DIR}/mysqlimport.conf"

if [ -z "$1" ]; then 
	echo "Usage: $0 BACKUP_PATH"
	exit 1
fi

if [ ! -f "${CONFIGFILE_NAME}" ]; then
  echo "Config file '${CONFIGFILE_NAME}' not found" >&2
  exit 1
fi

source "${CONFIGFILE_NAME}"

base_path=$1

if [ ! -d "$base_path" ]; then 
	echo "Folder $base_path is not exist" >&2
	exit 1
fi

LP="--login-path=\"${MYSQL_LOGIN_PATH}\""

mysql_exec_cmd="mysql {$LP} -Ns --batch"

function _import() {
	f=$1
	echo -en "proccesing $f ..."
	tar -xzf "$f" -O | $mysql_exec_cmd
	
	echo "ok"
}

find $base_path -type f -name '*.tar.gz' -exec _import {} \;
